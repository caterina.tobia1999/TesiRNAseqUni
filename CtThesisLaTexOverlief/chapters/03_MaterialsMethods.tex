%!TEX root = ../main.tex

\chapter{Materials and Methods:}
\label{chp:MATERIALSandMETHODS:}

The experiments were conducted in accordance with European Community Directive 2010/63 EU and were approved by the Animal Welfare Committee of the University of Trento, Superior Institute of Health and the Italian Ministry of Health. The animals were raised in a 12-hour light/dark cycle with food and water available ad libitum, and all efforts were made during the experiments to minimize animal suffering.

\section{Programming language: shell and R}
\paragraph{A shell} is a program with a command-line interface that runs inside a text-based terminal. The user types in a command, i.e., requests the execution of a program, create or edits files or new folders, it has been used with STAR to perform alignment. 
\paragraph{R} is an interpreted programming language for statistics and computational graphics, commonly used in the Rstudio \cite{RStudio} IDE (integrated development environment), which provides a set of tools to facilitate in programming. R has been used for differential gene expression analysis, network analysis, transcription factor analysis and gene set enrichment analysis. The code, the most salient graphs and related comments are contained in an interactive markdown document. \cite{markdown} 

\section{RNA-sequencing}
\subsection{Genotyping}
Following a litter, animals are divided into cages according to sex; in the following weeks, each mouse is given an earring with an identifying number. In this operation, a small ear flap is detached and used for genotyping. Genotyping is the process in which the genotype of an organism is identified using biological techniques: PCR and electrophoresis.

\begin{figure}[ht]
\begin{center}
    \includegraphics[height=7cm]{res/RNAseqWorkflow.png}
    \caption{RNA-sequencing workflow}
\end{center}
\vspace*{-\baselineskip}

\end{figure}   

\subsection{RNA extraction and quantification}
The cerebellum is isolated, cut, cryopreserved at -80\textdegree , then disintegrated using an homogenizer in RLT solution with $\beta$-mercaptoethanol (100ul mercapto + 10ml RLT). The lysate is transferred and centrifuged in a column that retains the genomic DNA. The same volume of ethanol 70\% is added to the supernatant, mixed by successive pipetting, then loaded twice into the Qiagen RNeasy column (MinElute spin column) by centrifuging for 15s. We proceed with successive washes always of 15s in which the eluate is thrown out hand by hand, the first one consists of adding 700 μl of RW1 buffer and in the next two 500 μl of RPE buffer is added in the end we centrifuge for 2 min. After changing eppendorf below the column, 25 μl of RNase-free water is added to the center of the membrane and centrifuged 1 min. This step can be repeated several times depending on the concentration of extracted RNA to be obtained; in our case it was done twice. The final eluate is put on ice and quantified at Nanodrop and the Qubit.  RIN values, i.e. RNA integrity number, were obtained with the Agilent 2100 Bioanalyzer instrument and only samples with RINs greater than 8 were chosen.

\subsection{Sequence reed with Illumina NGS}
Next-generation sequencing (NGS) is a high-throughput methodology that enables rapid sequencing of DNA or RNA samples. Sequencing is done by synthesis, that is, by adding complementary fluorescent nucleotides which are read with a laser and a detector, the amplified fragments are sequenced simultaneously in a massively parallel manner.  Many NGS platforms exist; we used Illumina HiSeq, which uses clonal bridge amplification, to build clusters of fragments attached to the flow cell, with reversible terminators, to sequence one base pair at a time. \cite{NGS}

Paired-end sequencing allows sequencing of both ends of a fragment while generating alignable, high-quality sequence data; sequences aligned as pairs of reads allow more accurate alignment by producing twice as many reads for the same amount of time and effort in library preparation. 

\paragraph{Library preparation} Preparation of NGS libraries, as shown in the figure \ref{fig:Illumina}, consist in the generation of a collection of fragments for sequencing, which are typically prepared by fragmenting genomic DNA or cDNA (retro-transcribed RNA) and ligating different adapters to both ends of the fragment. The specific adapters allow several cycles of clonal bridge amplification which consist of PCR extension followed by dissociation. The library is then sequenced to a read depth of 10 to 30 million reads per sample.

\begin{figure}[H]
\begin{center}
    \includegraphics[height=8cm]{res/NGS.png}
    \caption{NGS - Illumina \cite{ngsSeq}}
    \label{fig:Illumina}
\end{center}
\vspace*{-\baselineskip}
\end{figure}   

\subsection{STAR: Spliced Transcripts Alignment to a Reference}
STAR is one of several splice-aware alignment tools that has high accuracy and outperform other aligners in mapping speed, but it is memory intensive. The algorithm achieves this highly efficient mapping by performing a two-step process: seed searching followed by grouping, stitching and scoring. STAR has been used to perform trimming, mapping and counting; this application has been exploited by interfacing with the terminal bash within the R-Studio environment.
  
\paragraph{Adapter trimming and normalisation}
Illumina provides compressed fastq files for each samples in separate folders, the row files are trimmed for removal of library adapters, this steps include other normalisation procedure to increase sample quality.
\paragraph{Gene mapping}
The alignment process involves choosing an appropriate reference genome, in our case the GRCm39 mouse genome \cite{GRCm39}, with which to perform a splice-aware mapping using STAR, as in figure \ref{fig:STAR}. 

\begin{figure}[H]
\begin{center}
    \includegraphics[height=4.5cm]{res/STAR.png}
    \caption{STAR - Gene mapping \cite{STAR_fig}}
    \label{fig:STAR}
\end{center}
\vspace*{-\baselineskip}
\end{figure} 
\paragraph{Gene counting}
In parallel with the alignment, STAR quantifies reads that overlap with transcripts and provides a table of gene counts for each sample sequenced.

\subsection{DGEA, differential gene expression analysis}
Differential expression analysis involves performing a statistical analysis to decide whether, for a given gene, an observed difference in the read count is significant, that is, whether it is greater than what would be expected by random variation alone.
This enables the discovery of quantitative variations in individual genes, that is relative expression levels, among experimental groups. 

\paragraph{Build the matrices of count and Sample annotation}
The separate count data frames, one for each sample, containing counts per gene (rows) obtained with STAR, are merged into a final data frame called dfc containing counts per gene (rows) for each sample (column). A new data frame called mydirs is made with the name of the samples, it contains Condition, Sex, Age, Lane for each sample.
\paragraph{DESeq2} The standard differential expression analysis steps are wrapped into a single function, DESeq2. This function takes as input the two data frames dfc and mydirs and the conditions: Genotype was used as the difference between samples, Sex as covariant, ID was used to collapse sample replicates. DESeq2 results give as output the  diffentially expressed gene table with log2 fold changes, p-values and adjusted p-values for each gene mapped. EdgeR can also be used for the same purpose.
\paragraph{Gene annotation} 
The Get Bio Mart function (getBM) of Ensembl was used to retrieve gene information. For example this function was used to retrieve gene name and description, with the attribute "external\_gene\_name" and "description", the "gene\_biotype" was used to filter our DEGs in order to keep only protein-coding genes, finally Get Bio Mart was used to retrieve the UTR sequence of each gene for TFs analysis. 

\subsection{Network analysis}
With network analysis the relationships between genes can be graphically represent. The centrality of a gene is correlated to its connectivity in the network, the degree of connectivity of a gene and its influence in the network are directly proportional.

\subsection{Transcription factor analysis}
Using a window of 500 nucleotides upstream of each gene, information retrieved with the getBM or getSequence function, and applying pattern matching with the motifEnrichment function, which takes as input the retrieved sequence and the PWMLogn.mm9.MotifDb.Mmus, we identify which transcription factors have enriched scores in the promoters and UTRs of all up- and down-regulated genes. \\The PWMLogn.mm9.MotifDb.Mmus are the PWMs of mouse TFs, the position weight matrices that express the probability that a nucleotide at a specific position belongs to that TF motif. Motifs are recurrent patterns in DNA; in the case of regulatory motifs that bind to transcription factors (TFs), this interaction is a mechanism for extensive regulation of transcription and gene expression, since a single TF can have a high impact on the expression of several genes.

\subsection{GSEA gene set enrichment analysis}
GSEA is a method to identify classes of genes or proteins that are over- or under-represented in a large set of genes or proteins, every set of genes is a priori defined and associated to a specific cellular function, process, component or pathway. The method uses statistical approaches, the hypergeometric test, to identify significantly enriched or depleted groups of genes. The genes in the list of differential expressed gene (output of DESeq2) are ranked based on the fold change, for each a priori defined gene set (ontology or pathway) the enrichment score (ES) is computed by increasing a running-sum statistic when a gene is in the gene set and decreasing it when it is not. \cite{GSEA} \\
The gene ontology, namely a set of concepts within a domain, can be divided in three knowledge domain: Molecular Function, that describe cellular activities, Biological Process, that describe larger processes, and Cellular Component, that inform about location. The same analysis can be used to infer pathways throw KEGG database, curated annotation of biological pathways: series of actions among molecules in a cell that leads to a product or a change in the cell.
RNA-seq data are correlated with gene sets in order to extract functional significance, that is, to understand which gene sets best summarize gene expression patterns. This provides a better understanding of the biological processes underlying the difference between samples, in this case between the cerebellum of autistic mice and their wild-type counterparts. 

% \newpage
\section{Graphs}
\subsection{PCA} Principal component analysis is commonly used to reduce dimensionality by projecting each data point onto only the first two principal components, preserving most of the variance in the data. The first principal component maximizes the variance of the projected data by making a weighted linear sum of the components, in our case the differentially expressed genes that determine the position of the samples in Cartesian space. As in figures \ref{fig:PCA} and \ref{fig:het2}
\subsection{SCREE plot}
A scree plot is a line plot of the eigenvalues of the principal components analysis, a downward curve, ordered from largest to smallest. In Figure \ref{fig:het2} in the absence of the outlier the total variance is explained by 13 principal components while with sample 333 the principal components used to achieve 100 percent explained variance are 15.
\subsection{QQ-plot} 
A  Quantile-quantile plot plot is a graphical method for comparing two probability distributions by plotting their quantiles against each other, in our case it has been used to plot the obtained distribution of p-values against the expected one. In Figure \ref{fig:het3} in the absence of the outlier the p-values are slightly inflated i.e., overestimated while with sample 333 the p-values of differentially expressed genes are drastically underestimated compared with what is expected. 

\subsection{MA-plot} 
The MA plot is an application of a Bland–Altman plot for visual representation of genomic data, it is meant to compare two groups and see how different the samples are in terms of read counts. The log2 fold-change indicates the mean expression level for each gene, each dot represents one gene, while gray dots represent no significant differences between control and CNTNAP-/- mice, blue dots represent up-regulated and down-regulated genes, in figure \ref{fig:MA}. 
\subsection{Volcano plot}
It is a scatter plot that allows rapid visual identification of the most significant changes in large datasets.
Plotting p-value versus fold-change draws in the upper ends the genes that are significant and underlie a high magnitude of change between case and control, the DEGs, on the left the significantly down-regulated genes and on the right the up-regulated genes. In Figure \ref{fig:volcano}, up-regulated genes are colored red, down-regulated genes are colored green, and insignificant genes are colored gray.

\subsection{Heatmap}
The Heat Map is a graphical representation of data where individual values contained in a matrix are represented by colors: In Figures \ref{fig:upNET1} and \ref{fig:downNET1} the colors are black, meaning that the two genes are connected by an arc, and ochre, which is that the two are not connected. 
\subsection{STRING Network plot}
STRING is a database of protein-protein interactions; interactions can be direct, i.e. physical associations, protein-protein, signalling, metabolic and gene regulation such as TF and miRNA, or indirect, i.e. functional associations, co-expression with different stimuli, at different time points or cellular conditions. The interactions are based on knowledge or predicted from computational prediction, from knowledge transfer between organisms, and from other (primary) databases. This database builds interactive graphs that can be downloaded or inspected on the online platform that allows filtering the information, known and predicted about the interaction i.e., the arcs between the nodes (genes) given as input, and colouring the nodes individually according to their membership in pathways or GOs.

\subsection{PWM Sequence logo plot}
The binding sites of TFs are often represented as consensus sequences, i.e., sequences that represent the result of multiple alignments of sequences with the goal of finding recurrent motifs among the sequences. The consensus may also be different from all input sequences since it presents only the most conserved element for each position. A position weight matrix (PWM) has one row for each symbol (4 for DNA/RNA nucleotide or 20 for protein amino acids) and one column for each position in the pattern. To construct a PWM a frequency matrix (PFM) is created counting the occurrences of each nucleotide at each position, then a position probability matrix (PPM) is created by dividing the nucleotide count at each position by the number of sequences, thereby normalising the values. Finally PWM are calculated as log likelihoods of PPM transformed using a background model (0.25 for nucleotides and 0.05 for amino acids).
\paragraph{Sequence Logos} graphical representation of consensus sequences, it consists of a stack of letters at each position whose relative size indicates their frequency in the sequences and the total height depicts the information content of the position, in bits.

Figures \ref{fig:PWM20up} and \ref{fig:PWM20down} list the top 20 transcription factors emerging from the mapping on the upstream regions of up-regulated and down-regulated genes, respectively, and for each TF, the name, sequence logo of the PWM, corresponding motif ID, raw score, p-value, and mapping percentage in the best motifs are presented.

This is followed by the figures of the transcription factors investigated namely\\ Klf4 (\ref{fig:PWMKlf4}), Tcfap2a/b/c (\ref{fig:PWMTfap2A}),Pax2/6 (\ref{fig:PWMPax}), Myc, Creb1 and Jun (\ref{fig:PWMMycJunCreb1}).

\subsection{Motive score plots}
The motifScores function \cite{motifScores} was used to obtain the raw scores at each position in the sequence, this function displays the motif scores for one or more sequences. 
The result of this function is a list of matrices, each element in the list corresponds to an input sequence, in our case, in figures \ref{fig:KIf4_map1} \ref{fig:KIf4_map2}, in the image on the left all up-regulated genes are listed while on the right only the first six, consequently, on the left we get a list of length 589 while on the right it is of length six. The score matrix is a matrix in which the rows correspond to the two strands, whose scores are concatenated one after the other, and the columns to the motifs. The sequences are drawn as lines and the scores are drawn as rectangles on either side of the line, where the top and bottom correspond to the two strands. The width of the base of the rectangle corresponds to the width of the pattern and the height to the log(score) of the pattern that is positive and greater than the cutoff parameter, if specified. All scores have the same y-axis, so bar heights are comparable between sequences and motifs.


\subsection{Ridge plot}
A Ridgelineplot (formerly called Joyplot) allows to study the distribution of a numeric variable for several groups. In this thesis project it has been used to check the distribution of gene ontology and pathways based on their normalized enriched score, the peaks are coloured based on the significance. Figure \ref{fig:ridge} used the Ridgelineplot to summarize the results of the gene set enrichment analysis on ontologies and pathways.
\subsection{Enriched Score plot}
An “enrichment plot” provides a graphical view of the enrichment score (ES) for a gene set, the green line represents the running ES for a given GO as the analysis goes down the ranked list, while the black vertical lines represent the DEGs that belongs to the gene set, the value at the peak is the final ES. The GO or the pathway is activated if the ES is positive while it is suppressed if the ES is negative, that is when the majority of the DEGs that belongs to the gene set are on the right tail. In the case of a multiple ES plot, as in figure \ref{fig:3GSEA} each gene set has a different color. 
\subsection{Dot plot}
The dot plot can be used to visualize multiple sets of enriched genes in a single figure. Dividing by sign means separating activated gene sets, with NES > 0, from suppressed gene sets, with NES < 0.  With showCategory, on the other hand, the number of gene sets to be drawn is determined. The dot plots were used in the network analysis to characterize the ontologies of nodes belonging to cliques, figures \ref{fig:upNET5} and \ref{fig:downNET5}. It was then used in the analysis of ontologies divided by knowledge tomorrows and sign in figures \ref{fig:dotGO}, and to characterize the NESs of selected ontologies by keywords, figures \ref{fig:GO1}, \ref{fig:GO2} and \ref{fig:GO3}. In the end it was used to distinguish activated pathways from suppressed pathways in figure \ref{fig:dotKeg}.
\subsection{Emap plot}
Cnetplot and enrichMap (Emap) are good to visualize relationship among enriched gene sets and the corresponding core genes. The Emap plot was used to visualize the relationships between pathways identified as significant, figure \ref{fig:dotKeg2}.
\subsection{Pathview}
Pathview is a tool set for pathway based data integration and visualization, on pathway maps or drawings of relevant molecules, the up-regulated genes are coloured in red while the down-regulated ones are marked in green. \cite{pathview}\\
The following pathways were drawn with this function: ribosome (figure \ref{fig:RIBOSOME}), oxidative phosphorylation (figure \ref{fig:PHOSPHORYLATION}), nucleotide excision repair (figure \ref{fig:NER}), cytokine-cytokine receptor interaction (figure \ref{fig:cytokine}), PI3K-AKT (figure \ref{fig:PI3K}), neuroactive ligand (figure \ref{fig:neuroactive}), cholinergic synapse (figure \ref{fig:synapse}), axon guidance (figure \ref{fig:guidance}).




